<?php

namespace App\Http\Controllers;

use App\Checkin;
use Illuminate\Http\Request;

class UserController extends Controller
{

	public function locations_save(Request $request) {

		$checkin = new Checkin;
		
		$checkin->user_id = $request->user_id ;


		//$ifconfig =  shell_exec ("iwconfig");

		//dd($ifconfig); 


		//return $_SERVER['REMOTE_ADDR'];

		//$ip = '14.98.30.130';

		//$location = json_decode(file_get_contents('http://ipinfo.io/'.$ip.'/json'));

		//dd($location);

		$checkin->city = $request->city;

		$checkin->state = $request->state;

		$checkin->country = $request->county;


		$checkin->latitude = $request->latitude;

		$checkin->longitude = $request->longitude;

		$checkin->save();

		return response()->json($checkin);

	}

	public function locations_update(Request $request, $id) {

		$checkin = Checkin::find($id);

		$checkin->user_id = $request->user_id ;

		$checkin->latitude = $request->latitude ;

		$checkin->longitude = $request->longitude ;

		$checkin->save();

		return response()->json($checkin);
	}

	public function locations_show($id) {

		 $checkin = Checkin::find($id);

		 return response()->json($checkin);
	}

	public function locations_delete($id) {

		$checkin = Checkin::findOrFail($id);

		$checkin->delete();

		return response()->json('deleted');
	}

}
