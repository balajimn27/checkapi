<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/checkin/', 'UserController@locations_save');

Route::put('/checkin/{id}', 'UserController@locations_update');

Route::get('/checkin/{id}', 'UserController@locations_show');

Route::delete('/checkin/{id}', 'UserController@locations_delete');

